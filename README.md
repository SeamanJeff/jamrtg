# README #

README for jamrtg (Jeff's Alpine Linux based MRTG)

### What is this ? ###

A way to get MRTG set up very quickly and to run MRTG while using minimal resources.

It uses Docker, Docker-compose, Alpine Linux, Lighttpd and of course MRTG, the Multi-Router Traffic Grapher.  The home page for MRTG is [http://oss.oetiker.ch/mrtg/](http://oss.oetiker.ch/mrtg/).


### How do I get set up? ###

First clone the respository.
~~~~
root@osprey:/build# git clone https://SeamanJeff@bitbucket.org/SeamanJeff/jamrtg.git
Cloning into 'jamrtg'...
remote: Counting objects: 10, done.
remote: Compressing objects: 100% (7/7), done.
remote: Total 10 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (10/10), done.
Checking connectivity... done.
~~~~
Now install docker-compose, if you don't already have it.
~~~~
root@osprey:/build/jamrtg# curl -L --fail \
https://github.com/docker/compose/releases/download/1.13.0/run.sh \
> /usr/local/bin/docker-compose
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   582    0   582    0     0   2095      0 --:--:-- --:--:-- --:--:--  2101
100  1411  100  1411    0     0   2979      0 --:--:-- --:--:-- --:--:--  2979
root@osprey:/build/jamrtg# sudo chmod +x /usr/local/bin/docker-compose
~~~~
Update the .env file to control which ip the container's web server is mapped to on the docker machine's network interface:
~~~~
root@osprey:/build/jamrtg# more .env
COMPOSE_PROJECT_NAME=jamrtg
MRTG_IP=192.168.44.100
root@osprey:/build/jamrtg#
~~~~
Alternatively you could update the docker-compose.yaml file to map it to a port other than 80.


Build the images.
~~~~
root@osprey:/build/jamrtg# docker-compose build
Unable to find image 'docker/compose:1.13.0' locally
1.13.0: Pulling from docker/compose
709515475419: Pull complete
a3bdaecc7369: Pull complete
7f2b2d814adf: Pull complete
Digest: sha256:bb540711ee1a63ead55fc825c6f3989794a738207c1cd44f2265929354734af6
Status: Downloaded newer image for docker/compose:1.13.0
Building mrtg
Step 1 : FROM gliderlabs/alpine:3.4
3.4: Pulling from gliderlabs/alpine
4adb3aeab9a5: Pull complete
a3ed95caeb02: Pull complete
Digest: sha256:52dae13aad2ba9bd62685590bc415b7fc59080dd0064615de0a4577144a69b87
Status: Downloaded newer image for gliderlabs/alpine:3.4
 ---> 27e84ebd243e
 ---> Running in 733d873dc4c3
 ---> 3c6051a2afba
Removing intermediate container 733d873dc4c3
Step 3 : RUN apk add --update mrtg
 ---> Running in 06b95a2fd64e
fetch http://alpine.gliderlabs.com/alpine/v3.4/main/x86_64/APKINDEX.tar.gz
fetch http://alpine.gliderlabs.com/alpine/v3.4/community/x86_64/APKINDEX.tar.gz
(1/6) Installing perl (5.22.2-r0)
(2/6) Installing libpng (1.6.21-r0)
(3/6) Installing freetype (2.6.3-r0)
(4/6) Installing libjpeg-turbo (1.4.2-r0)
(5/6) Installing gd (2.2.4-r0)
(6/6) Installing mrtg (2.17.4-r3)
Executing busybox-1.24.2-r13.trigger
OK: 51 MiB in 17 packages
 ---> 395b1eba2ad5
Removing intermediate container 06b95a2fd64e
Step 4 : RUN echo '*/5     *       *       *       *       \
/usr/bin/mrtg /etc/mrtg/mrtg.cfg' >> /etc/crontabs/root
 ---> Running in b81c07e59599
 ---> de11c9682602
Removing intermediate container b81c07e59599
Step 5 : VOLUME /etc/mrtg
 ---> Running in 4555f7afced3
 ---> 4a59424e2170
Removing intermediate container 4555f7afced3
Step 6 : VOLUME /var/www/mrtg
 ---> Running in 4955b828e4b5
 ---> 65127bb7875b
Removing intermediate container 4955b828e4b5
Step 7 : CMD crond -f
 ---> Running in b11023d41dfa
 ---> 26c4cfbb1d50
Removing intermediate container b11023d41dfa
Successfully built 26c4cfbb1d50
Building lighttpd
Step 1 : FROM gliderlabs/alpine:3.4
 ---> 27e84ebd243e
Step 2 : MAINTAINER Jeff Dickens <dreamgear@gmail.com>
 ---> Using cache
 ---> 3c6051a2afba
Step 3 : RUN apk add --update lighttpd
 ---> Running in 69ca39eccb05
fetch http://alpine.gliderlabs.com/alpine/v3.4/main/x86_64/APKINDEX.tar.gz
fetch http://alpine.gliderlabs.com/alpine/v3.4/community/x86_64/APKINDEX.tar.gz
(1/5) Installing libbz2 (1.0.6-r5)
(2/5) Installing libev (4.22-r0)
(3/5) Installing lua5.1-libs (5.1.5-r1)
(4/5) Installing pcre (8.38-r1)
(5/5) Installing lighttpd (1.4.39-r3)
Executing lighttpd-1.4.39-r3.pre-install
Executing busybox-1.24.2-r13.trigger
OK: 6 MiB in 16 packages
 ---> 96bcb77cac0a
Removing intermediate container 69ca39eccb05
Step 4 : RUN sed -i -e 's/#    "mod_alias"/    "mod_alias"/' /etc/lighttpd/lighttpd.conf
 ---> Running in ee5cd35b83e3
 ---> 07b70349ef34
Removing intermediate container ee5cd35b83e3
Step 5 : RUN echo 'alias.url = ("/mrtg" => "/var/www/mrtg")' >> /etc/lighttpd/lighttpd.conf
 ---> Running in 3ec52a64d406
 ---> 6b3bdac25af8
Removing intermediate container 3ec52a64d406
Step 6 : VOLUME /var/www/mrtg
 ---> Running in c27ad5cea044
 ---> dc798fce0941
Removing intermediate container c27ad5cea044
Step 7 : EXPOSE 80
 ---> Running in 6b149f5b2cc4
 ---> 4c14fba2fa64
Removing intermediate container 6b149f5b2cc4
Step 8 : CMD /usr/sbin/lighttpd -D -f /etc/lighttpd/lighttpd.conf
 ---> Running in 4faed6a3c564
 ---> 3276cb0e6911
Removing intermediate container 4faed6a3c564
Successfully built 3276cb0e6911
root@osprey:/build/jamrtg#
~~~~
Now there should be two images, one for mrtg itself, and one for lighttpd.
~~~~
root@osprey:/build/jamrtg# docker images | grep mrtg
mrtg-lighttpd       latest              3276cb0e6911        9 minutes ago       6.932 MB
mrtg                latest              26c4cfbb1d50        9 minutes ago       49.42 MB
root@osprey:/build/jamrtg#
~~~~
Create directories to which the containers will map their configuration and data volumes.
~~~~
root@osprey:/build/jamrtg# mkdir /usr/local/etc/mrtg
root@osprey:/build/jamrtg# mkdir /usr/local/etc/mrtg/www
root@osprey:/build/jamrtg#
~~~~
Here we run a container pretty much the same way docker-compose will run mrtg, so we can configure mrtg.
~~~~
root@osprey:/build/jamrtg# docker run -ti  -rm -v \ /usr/local/etc/mrtg:/etc/mrtg -v \
/usr/local/etc/mrtg/www:/var/mrtg/www mrtg ash
/ #
~~~~
Now inside the container we run cfgmaker as many times as needed, storing the generated configs in /etc/mrtg, which is mapped to the configuration directory outside the container which we created above.
~~~~
/ # cfgmaker --output=/etc/mrtg/abc-rtr.cfg public@abc-rtr
--base: Get Device Info on public@abc-rtr:
--base: Vendor Id: Unknown Vendor - 1.3.6.1.4.1.664.1.1163
--base: Populating confcache
--base: Get Interface Info
--base: Walking ifIndex
--snpd:   public@abc-rtr: -> 1 -> ifIndex = 1
--snpd:   public@abc-rtr: -> 2 -> ifIndex = 2
--snpd:   public@abc-rtr: -> 3 -> ifIndex = 3
--snpd:   public@abc-rtr: -> 4 -> ifIndex = 4
.
.
--snpd:   public@abc-rtr: -> 32 -> ifSpeed = 100000000
--snpd:   public@abc-rtr: -> 33 -> ifSpeed = 100000000
--base: Writing /etc/mrtg/abc-rtr.cfg
/ #

~~~~

~~~~
/ # cfgmaker --output=/etc/mrtg/abc-firewall.cfg public@10.10.10.1
--base: Get Device Info on public@10.10.10.1:
--base: Vendor Id: Unknown Vendor - 1.3.6.1.4.1.8741.1
--base: Populating confcache
--base: Get Interface Info
--base: Walking ifIndex
--snpd:   public@10.10.10.1: -> 1 -> ifIndex = 1
--snpd:   public@10.10.10.1: -> 2 -> ifIndex = 2
--snpd:   public@10.10.10.1: -> 3 -> ifIndex = 3
--snpd:   public@10.10.10.1: -> 4 -> ifIndex = 4
.
.
--snpd:   public@10.10.10.1: -> 10 -> ifSpeed = 0
--snpd:   public@10.10.10.1: -> 11 -> ifSpeed = 0
--base: Writing /etc/mrtg/abc-firewall.cfg
/ #
~~~~
Still inside the container, we create our mrtg.cfg file, including the generated configs from above.
~~~~
/ # cat > /etc/mrtg/mrtg.cfg
WorkDir: /var/www/mrtg

### Global Defaults

#  to get bits instead of bytes and graphs growing to the right
Options[_]: growright, bits

EnableIPv6: no

Include: /etc/mrtg/abc-firewall.cfg

Include: /etc/mrtg/abc-rtr.cfg

Include: /etc/mrtg/abc-switch.cfg
/ #
~~~~
Check that all the files we expect are there.
~~~~
/ # ls /etc/mrtg
abc-firewall.cfg  abc-switch.cfg    www
abc-rtr.cfg       mrtg.cfg
/ #
~~~~
Now we fire up mrtg manually.  It will complain a lot the first time it runs.  Then run it a second time and it should run clean.
~~~~
/ # mrtg /etc/mrtg/mrtg.cfg
2017-05-25 15:53:18, Rateup WARNING: /usr/bin/rateup could not read the primary log file for 10.10.10.1_1
2017-05-25 15:53:18, Rateup WARNING: /usr/bin/rateup The backup log file for 10.10.10.1_1 was invalid as well
2017-05-25 15:53:18, Rateup WARNING: /usr/bin/rateup Can't rename 10.10.10.1_1.log to 10.10.10.1_1.old updating log file
.
.
/ #
/ #
/ # mrtg /etc/mrtg/mrtg.cfg
/ #
/ #
~~~~
Finally run indexmaker to generate our index.html file.  Then exit the container (which will then be remove since we started it with --rm).
~~~~
/ # indexmaker --output=/var/www/mrtg/index.html /etc/mrtg/mrtg.cfg
/ #
~~~~
Now bring up the two containers with docker-compose, and you should now be able to view the web page at the ip address we defined above.
~~~~
root@osprey:/build/jamrtg# docker-compose up -d
Creating network "jamrtg_default" with the default driver
Creating jamrtg_lighttpd_1 ...
Creating jamrtg_mrtg_1 ...
Creating jamrtg_lighttpd_1
Creating jamrtg_mrtg_1 ... done
root@osprey:/build/jamrtg#
~~~~
If you want, you can edit the generated config files to add human-friendly names.  To see these changes reflected in the index, run indexmaker in the mrtg container with docker exec.
~~~~
root@osprey:/build/jamrtg# docker exec -ti jamrtg_mrtg_1 indexmaker --output=/var/www/mrtg/index.html /etc/mrtg/mrtg.cfg
root@osprey:/build/jamrtg#
~~~~